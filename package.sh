#!/bin/bash

VERSION=`grep '<version>' plugin/plugin.xml | sed "s@.*<version>\(.*\)</version>.*@\1@"`

mkdir HelloAgainConnector
cp -R plugin/* HelloAgainConnector

zip -r "helloagain-shopware5-$VERSION.zip" HelloAgainConnector -x "*.DS_Store"

rm -rf HelloAgainConnector
