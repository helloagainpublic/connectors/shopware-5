<?php

namespace HelloAgainConnector\Service;

use Shopware\Models\Customer\Customer;

class CustomerHelper extends AbstractHelper
{
    public function authenticate($email, $password)
    {
        $customer = $this->loadCustomerByEmail($email);

        if (
            $customer && $this->container->get('passwordencoder')->isPasswordValid(
                $password,
                $customer->getPassword(),
                $customer->getEncoderName()
            )
        ) {
            return $customer;
        }

        return false;
    }

    public function loadCustomerById($customerId)
    {
        return $this->container->get('models')->getRepository(Customer::class)->find($customerId);
    }

    public function loadCustomerByEmail($email)
    {
        return $this->container->get('models')->getRepository(Customer::class)->findOneBy(['email' => $email]);
    }

    public function saveHelloagainId($customer, $helloagainId)
    {
        $customerAttribute = $customer->getAttribute();
        $customerAttribute->setHelloagainId($helloagainId);

        $this->container->get('models')->persist($customerAttribute);
        $this->container->get('models')->flush($customerAttribute);
        $this->container->get('models')->refresh($customerAttribute);
    }

    public function initHelloagainId($customer)
    {
        $customerAttribute = $customer->getAttribute();

        /*if (strlen($customerAttribute->getHelloagainId()) > 0 && $customerAttribute->getHelloagainId() != 'INIT') {
            throw new \Exception('Helloagin ID already set');
        }*/

        $this->saveHelloagainId($customer, 'INIT');
    }

    public function isHelloagainUser($customer)
    {
        return strlen($customer->getAttribute()->getHelloagainId()) > 0;
    }

    /**
     * Logs in the user for the given customer object
     *
     * @param Customer $customer
     * @return array|bool
     * @throws \Exception
     */
    public function setCustomerDataAsLoggedIn($customer)
    {
        Shopware()->Front()->Request()->setPost('email', $customer->getEmail());
        Shopware()->Front()->Request()->setPost('passwordMD5', $customer->getPassword());
        return $this->container->get('modules')->Admin()->sLogin(true);
    }
}
