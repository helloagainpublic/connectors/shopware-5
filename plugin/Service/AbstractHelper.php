<?php

namespace HelloAgainConnector\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Shopware\Components\Plugin\ConfigReader;
use Shopware\Models\Shop\Shop;
use Shopware\Bundle\StoreFrontBundle\Service\ContextServiceInterface;

class AbstractHelper
{

    /**
     * @var ConfigReader
     */
    protected $configReader;

    /**
     * @var ContainerInterface;
     */
    protected $container;

    /**
     * @var array
     */
    protected $data = null;

    /**
     * @var Shop;
     */
    private $shop;

    /**
     * @var ContextServiceInterface
     */
    private $contextService;

    public function __construct(
        ConfigReader $configReader,
        ContainerInterface $container,
        ContextServiceInterface $contextService = null
    ) {
        $this->configReader = $configReader;
        $this->container = $container;
        $this->contextService = $contextService ?: Shopware()->Container()->get(ContextServiceInterface::class);
    }

    /**
     * Get the Shopware config for a Shopware shop
     *
     * @param  string $key
     * @param  string|null $default
     * @return array
     */
    public function getConfig($key = null, $default = null)
    {
        if (is_null($this->data)) {

            if (Shopware()->Container()->has('shop')) {
                $shop = Shopware()->Shop();
            } else {
                $shop = null;
                if ($this->shop) {
                    $shop = $this->shop;
                }
            }

            // get config for shop or for main if shopid is null
            $parts = explode('\\', __NAMESPACE__);
            $name = array_shift($parts);
            $this->data = $this->configReader->getByPluginName($name, $shop);
        }

        if (!is_null($key)) {
            return isset($this->data[$key]) ? $this->data[$key] : $default;
        }

        return $this->data;
    }

    public function setShop(Shop $shop)
    {
        $this->shop = $shop;
    }

    public function getShopUrl()
    {
        $shop = $this->contextService->getShopContext()->getShop();
        $shopUrl = 'http://' . $shop->getHost() . $shop->getUrl();

        if ($shop->getSecure()) {
            $shopUrl = 'https://' . $shop->getHost() . $shop->getUrl();
        }
        return $shopUrl;
    }

    public function getUrl($path = '', $params = null)
	{
		return $this->getShopUrl() . "/{$path}" . (is_array($params) ? '?' . http_build_query($params) : '');
	}
}
