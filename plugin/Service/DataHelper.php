<?php

namespace HelloAgainConnector\Service;

use Shopware\Models\Shop\Shop;

class DataHelper extends AbstractHelper
{
    const XML_CONFIG_API_ENDPOINT = 'helloagain_apiUri';
    const XML_CONFIG_API_KEY = 'helloagain_apiKey';
    const XML_CONFIG_CLIENT_ID = 'helloagain_clientId';
    const XML_CONFIG_CLIENT_SECRET = 'helloagain_clientSecret';
    const XML_CONFIG_REDIRECT_URI = 'helloagain_redirectUri';
    const XML_CONFIG_JWT_KEY = 'helloagain_jwtKey';
    const XML_CONFIG_TP_KEY = 'helloagain_tpKey';
    const XML_CONFIG_ORDER_STATUS = 'helloagain_OrderStatus';
    const XML_CONFIG_LOGIN_PAGE= 'helloagain_loginPage';
    const USER_AUTHENTICATE = '/users/authenticate/';
    const USER_DATA = '/users/';
    const USER = '/user/';
    const RECEIPTS = '/receipts/';
    const EXTERNAL_IDENTIFIER = '/external-identifier/';
    const THIRD_PARTY_AUTHENTICATION = '/third-party-authentication/';
    const HELLOAGAIN_LOGIN = 'helloagain_login';
    const DEFAULT_LOGIN = 'default_login';

    public function getApiEndpoint()
    {
        return $this->getConfig(self::XML_CONFIG_API_ENDPOINT);
    }

    public function getApiKey()
    {
        return $this->getConfig(self::XML_CONFIG_API_KEY);
    }

    public function getClientId()
    {
        return $this->getConfig(self::XML_CONFIG_CLIENT_ID);
    }

    public function getClientSecret()
    {
        return $this->getConfig(self::XML_CONFIG_CLIENT_SECRET);
    }

    public function getRedirectUri()
    {
        return $this->getConfig(self::XML_CONFIG_REDIRECT_URI);
    }

    public function getJwtKey()
    {
        return $this->getConfig(self::XML_CONFIG_JWT_KEY);
    }

    public function getTpKey()
    {
        return $this->getConfig(self::XML_CONFIG_TP_KEY);
    }

    public function getOrderStatuses()
    {
        return $this->getConfig(self::XML_CONFIG_ORDER_STATUS);
    }

    public function getLoginPage()
    {
        return $this->getConfig(self::XML_CONFIG_LOGIN_PAGE);
    }

    public function getUserAuthenticateEndpoint($token)
    {
        return $this->getApiEndpoint() . self::USER_AUTHENTICATE . $token;
    }

    public function getReceiptsEndpoint()
    {
        return $this->getApiEndpoint() . self::RECEIPTS;
    }

    public function getUserByEmailEndpoint($email)
    {
        return $this->getApiEndpoint() . self::USER_DATA . "?email=" . $email;
    }

    public function getUserByIdEndpoint($userId)
    {
        return $this->getApiEndpoint() . self::USER . $userId . self::EXTERNAL_IDENTIFIER . $this->getTpKey();
    }

    public function getUserByHlaIdEndpoint($hlaId) 
    {
        return $this->getApiEndpoint() . self::USER_DATA . $hlaId . self::THIRD_PARTY_AUTHENTICATION . $this->getTpKey();
    }

    public function getHelloagainApiData($url, $params = [])
    {
        $curl = curl_init();
        $headers = array(
            "Content-Type: application/json",
            "helloagain-api-key:" . $this->getApiKey()
        );

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($curl, CURLOPT_AUTOREFERER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        if (count($params) > 0) {
            curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        }

        try {
            $response = curl_exec($curl);
        } catch (\Exception $ex) {
            return false;
        }
        $httpCode = intval(curl_getinfo($curl, CURLINFO_HTTP_CODE));
        curl_close($curl);

        $response = json_decode($response, true);
        if ($httpCode >= 200 && $httpCode < 300) {
            Shopware()->Container()->get('corelogger')->info("HelloAgain: [URL $url] [HTTP $httpCode] " . json_encode($response));
            return $response;
        }

        Shopware()->Container()->get('corelogger')->error("HelloAgain: [URL $url]  [HTTP $httpCode] " . json_encode($response));

        return false;
    }

    public function getUserData($accessToken)
    {
        return $this->getHelloagainApiData($this->getUserAuthenticateEndpoint($accessToken));
    }

    public function getUserDataByEmail($email)
    {
        return $this->getHelloagainApiData($this->getUserByEmailEndpoint($email));
    }

    public function getUserDataByUserId($userId)
    {
        return $this->getHelloagainApiData($this->getUserByIdEndpoint($userId));
    }

    public function getUserDataByHlaId($hlaId)
    {
        return $this->getHelloagainApiData($this->getUserByHlaIdEndpoint($hlaId));
    }

    public function sendOrderData($params)
    {
        return $this->getHelloagainApiData($this->getReceiptsEndpoint(), $params);
    }

    public function getRandomCode($length = 20)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);

        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    public function generateOauth2Code($customerId)
    {
        $code = $this->getRandomCode();

        /** @var HelloAgainConnector\Models\OauthDataRepository $oauthDataRepository */
        $oauthDataRepository = $this->container->get('models')->getRepository(
            \HelloAgainConnector\Models\OauthData::class
        );

        $oauthDataRepository->create(
            $customerId,
            Shopware()->Session()->get('hlaOauth2Scope'),
            $code
        );

        return $code;
    }

    public function generateOauth2Token($customer)
    {
        $tokenData = [
            "iss" => 'helloagain',
            "aud" => "helloagain.at",
            "exp" => strtotime(date('Y-m-d', strtotime('+1 year'))),
            "iat" => time(),
            "sub" => $customer->getId(),
            "nonce" => $this->getRandomCode(10),
            "first_name" => $customer->getFirstname(),
            "last_name" => $customer->getLastname(),
            "email" => $customer->getEmail(),
            "tp_key" => $this->getTpKey(),
        ];

        $jwt = new \Firebase\JWT\JWT;

        return $jwt->encode($tokenData, $this->getJwtKey(), 'HS256');
    }
}
