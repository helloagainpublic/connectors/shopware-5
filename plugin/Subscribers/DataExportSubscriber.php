<?php

namespace HelloAgainConnector\Subscribers;

use Enlight\Event\SubscriberInterface;
use HelloAgainConnector\Models\OrderData;
use Shopware\Models\Order\Order as OrderModel;

class DataExportSubscriber implements SubscriberInterface
{
    protected $container;
    protected $modelManager;
    protected $helper;
    protected $customerHelper;

    public function __construct()
    {
        $this->container = Shopware()->Container();
        $this->modelManager = $this->container->get('models');
        $this->helper = $this->container->get('hla.datahelper');
        $this->customerHelper = $this->container->get('hla.customerhelper');
    }

    public static function getSubscribedEvents()
    {
        return [
            'Shopware_CronJob_HelloAgainDataExport' => 'onDataExport'
        ];
    }

    public function onDataExport(\Shopware_Components_Cron_CronJob $job)
    {
        Shopware()->Container()->get('corelogger')->info("HelloAgain: onDataExport");
        try {
            $orderDataRepository = $this->modelManager->getRepository(OrderData::class);

            $orderDataToExport = $orderDataRepository->findBy(['syncedAt' => null]);

            foreach ($orderDataToExport as $orderData) {
                $order = $this->modelManager->getRepository(OrderModel::class)->findOneBy([
                    'number' => $orderData->getOrderId()
                ]);
                $orderId = $orderData->getOrderId();
                $customer = $order->getCustomer();
            
                $userData = $this->helper->getUserDataByUserId($customer->getId());

                if (isset($userData['id'])) {
                    $helloagainId = $userData['id'];
                    $this->customerHelper->saveHelloagainId($customer, $helloagainId);
                } else {
                    Shopware()->Container()->get('corelogger')->error("HelloAgain: Order Export: No helloagainId, Order [$orderId]");
                    continue;
                }

                foreach ($order->getDetails() as $orderItem) {
                    $orderedProducts[] = [
                        'identifier' => $orderItem->getArticleNumber(),
                        'name' => $orderItem->getArticleName(),
                        'price' => $orderItem->getPrice(),
                        'quantity' => $orderItem->getQuantity()
                    ];
                }

                $params = [
                    'user_id' => $helloagainId,
                    'time_and_date' => $order->getOrderTime()->format(\DateTime::ATOM),
                    'receipt_nr' => $order->getNumber(),
                    'product_instances' => $orderedProducts,
                    'total' => $order->getInvoiceAmount()
                ];

                $dataJson = json_encode($params);
                $response = $this->helper->sendOrderData($dataJson);

                $succeeded = isset($response['time_and_date']);

                $orderDataRepository->saveSyncInfo($orderData, $succeeded);
                Shopware()->Container()->get('corelogger')->info("HelloAgain: Order Export: SUCCESS - helloID [$helloagainId], Order [$orderId]");
            }
            
        } catch (\Exception $e) {
            $message = $e->getMessage();
            Shopware()->Container()->get('corelogger')->error("HelloAgain: Order Export: ERROR [$message]");
        }
    }
}
