<?php

namespace HelloAgainConnector\Subscribers;

use Enlight\Event\SubscriberInterface;
use Shopware\Models\Order\Order as OrderModel;
use HelloAgainConnector\Models\OrderData;

class OrderSubscriber implements SubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            'Shopware_Modules_Order_SaveOrder_ProcessDetails' => 'onOrderCreate'
        ];
    }

    public function onOrderCreate(\Enlight_Event_EventArgs $args)
    {
        $orderNumber = $args->getSubject()->sOrderNumber;

        $order = Shopware()->Container()->get('models')->getRepository(OrderModel::class)->findOneBy([
            'number' => $orderNumber
        ]);

        $orderStatusId = $order->getOrderStatus()->getId();

        $customerId = null;
        $helloagainId = 0;
        $isHlaOrderStatus = 0;

        if ($order instanceof OrderModel) {

            $orderNumber = $order->getNumber();
            $customer = $order->getCustomer();
            
            if ($customer) {
                $customerId = $customer->getId();
                $helloagainId = $customer->getAttribute()->getHelloagainId();
                $isHlaOrderStatus = in_array($orderStatusId, Shopware()->Container()->get('hla.datahelper')->getOrderStatuses());

                if ($isHlaOrderStatus && strlen($helloagainId) > 0) {
                    Shopware()->Container()->get('models')->getRepository(OrderData::class)->create($orderNumber, $helloagainId != 'INIT' ? $helloagainId : null);
                    Shopware()->Container()->get('corelogger')->info("HelloAgain: Order status change [$orderNumber] written to the DB");
                    return;
                }
            }
        }

        Shopware()->Container()->get('corelogger')->error("HelloAgain: Order status change [$orderNumber] NOT written to the DB - customerId: $customerId, isHlaUser: $helloagainId, isHlaOrderStatus: $isHlaOrderStatus");
    }
}
