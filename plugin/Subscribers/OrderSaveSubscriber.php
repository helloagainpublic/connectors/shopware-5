<?php

namespace HelloAgainConnector\Subscribers;

use Shopware\Models\Order\Order as OrderModel;
use Doctrine\ORM\Events;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\PreUpdateEventArgs;

class OrderSaveSubscriber implements EventSubscriber
{
    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
        return [Events::preUpdate];
    }

    public function preUpdate(PreUpdateEventArgs $eventArgs)
    {
        $order = $eventArgs->getEntity();

        if (!($order instanceof OrderModel)) {
            return;
        }

        // Order or payment status changed?
        if (
            !$eventArgs->hasChangedField('paymentStatus')
            && !$eventArgs->hasChangedField('orderStatus')
        ) {
            return;
        }

        $orderStatusId = $eventArgs->hasChangedField('orderStatus') ? 
            $eventArgs->getNewValue('orderStatus')->getId() : $order->getOrderStatus()->getId();

        $customerId = null;
        $helloagainId = 0;
        $isHlaOrderStatus = 0;

        $orderNumber = $order->getNumber();
        $customer = $order->getCustomer();
        
        if ($customer && $customer->getAttribute()) {
            $customerId = $customer->getId();
            $helloagainId = $customer->getAttribute()->getHelloagainId();

            $orderStatuses = Shopware()->Container()->get('hla.datahelper')->getOrderStatuses();
            $isHlaOrderStatus = in_array($orderStatusId, $orderStatuses);
            Shopware()->Container()->get('corelogger')->error("HelloAgain: Order status change [$orderNumber] - orderStatusId: $orderStatusId, orderStatuses: " . implode(',', $orderStatuses));

            if ($isHlaOrderStatus && strlen($helloagainId) > 0) {
                $isAlreadyAdded = $eventArgs->getEntityManager()->getConnection()->fetchAssoc('SELECT * FROM hla_order_data WHERE order_id = ?', [$orderNumber]);

                if (!$isAlreadyAdded) {
                    $orderData = [
                        'id' => null,
                        'order_id' => $orderNumber,
                        'helloagain_id' => $helloagainId != 'INIT' ? $helloagainId : null,
                        'synced_at' => null,
                        'sync_count' => null
                    ];
    
                    $eventArgs->getEntityManager()->getConnection()->insert('hla_order_data', $orderData);
                    Shopware()->Container()->get('corelogger')->error("HelloAgain: Order status change [$orderNumber] Status OK - written to the DB"); // error used instead of info to force writing
                } else {
                    Shopware()->Container()->get('corelogger')->error("HelloAgain: Order status change [$orderNumber] Status OK - already written to the DB");
                }
                    
            } else {
                Shopware()->Container()->get('corelogger')->error("HelloAgain: Order status change [$orderNumber] Status not OK - customerId: $customerId, isHlaUser: $helloagainId, isHlaOrderStatus: $isHlaOrderStatus");
            }
        } else {
            Shopware()->Container()->get('corelogger')->error("HelloAgain: Order status change [$orderNumber] NOT written to the DB - missing customer - customerId: $customerId, isHlaUser: $helloagainId, isHlaOrderStatus: $isHlaOrderStatus");
        }        
    }
}
