<?php
namespace HelloAgainConnector\Subscribers;

use Enlight\Event\SubscriberInterface;

class ActionPreDispatch implements SubscriberInterface
{
    private $container;
    private $helper;

    public function __construct()
    {
        $this->container = Shopware()->Container();
        $this->helper = $this->container->get('hla.datahelper');
    }

    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Action_PreDispatch_Frontend' => 'beforeLoad'
        ];
    }

    public function beforeLoad(\Enlight_Controller_ActionEventArgs $args)
    {
        $view = $args->getSubject()->View();
        if (Shopware()->Session()->get('hlaOauth2RedirectUri') != null) {
            $view->assign('isHlaPage', true);
        } else {
            $view->assign('isHlaPage', false);
        }

        if (isset($_GET['helloagain_token']) && strpos($_SERVER["REQUEST_URI"], '/api/') === false && strpos($_SERVER["REQUEST_URI"], 'hlacustomer/sessioninit') === false) {
            $redirectUri = ltrim(strtok($_SERVER["REQUEST_URI"], '?'), '/');
            $controller = $args->getSubject();
            $response = $controller->Response();
	        $response->setRedirect($this->helper->getUrl('hlacustomer/sessioninit',  ['redirect_uri' => $redirectUri, 'helloagain_token' => $_GET['helloagain_token']]), 302);
            return;
        }
    }
}
