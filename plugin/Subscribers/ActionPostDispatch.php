<?php
namespace HelloAgainConnector\Subscribers;

use Enlight\Event\SubscriberInterface;
use HelloAgainConnector\Service\DataHelper;

class ActionPostDispatch implements SubscriberInterface
{
    private $container;
    private $helper;

    public function __construct()
    {
        $this->container = Shopware()->Container();
        $this->helper = $this->container->get('hla.datahelper');
    }

    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Action_PostDispatch_Frontend' => 'aftrePageLoad'
        ];
    }

    public function aftrePageLoad(\Enlight_Controller_ActionEventArgs $args)
    {
        $availableActions = ['login', 'saveRegister'];
        if (!in_array($args->getRequest()->getActionName(), $availableActions, true) || !$args->getRequest()->isPost()) {
            return;
        }
        $userId = Shopware()->Session()->offsetGet('sUserId');
        $isDefaultLoginPage = $this->helper->getLoginPage() == DataHelper::DEFAULT_LOGIN ? true : false;

        if (Shopware()->Session()->get('hlaOauth2RedirectUri') != null && $userId && $isDefaultLoginPage) {
            $code = $this->helper->generateOauth2Code($userId);
            $controller = $args->getSubject();
            $response = $controller->Response();
            $response->setRedirect($this->helper->getUrl('hlaoauth2/redirect',  ['code' => $code]));
            return;
        }
    }
}