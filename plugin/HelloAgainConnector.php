<?php

namespace HelloAgainConnector;

use Doctrine\ORM\Tools\SchemaTool;

class HelloAgainConnector extends \Shopware\Components\Plugin
{
    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Action_PreDispatch_Frontend' => 'onFrontend'
        ];
    }

    public function install(\Shopware\Components\Plugin\Context\InstallContext $context)
    {
        try {
            $modelManager = $this->container->get('models');
            $schemaTool = new SchemaTool($modelManager);

            $noDrop = true;

            $sqls = $schemaTool->getUpdateSchemaSql(
                [
                    $modelManager->getClassMetadata(\HelloAgainConnector\Models\OauthData::class),
                    $modelManager->getClassMetadata(\HelloAgainConnector\Models\OrderData::class),
                ],
                $noDrop);

            $conn = $modelManager->getConnection();

            foreach($sqls as $sql) {
                $conn->executeQuery($sql);    
            }
            
            $service = $this->container->get('shopware_attribute.crud_service');
            $service->update('s_user_attributes', 'helloagain_id', 'string', [
                'label' => 'Helloagain ID',
                'supportText' => 'Automatically generated',
                'displayInBackend' => true,
                'readonly' => true
            ]);

        } catch (\Exception $e) {
            Shopware()->Container()->get('corelogger')->error('HelloAgain: Error creating DB Schema ' . $e->getMessage());
        }
    }

    public function onFrontend(\Enlight_Event_EventArgs $args)
    {
        $this->container->get('Template')->addTemplateDir(
            $this->getPath() . '/Resources/views/'
        );
    }
}