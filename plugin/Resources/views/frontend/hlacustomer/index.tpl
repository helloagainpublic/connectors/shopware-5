{extends file="frontend/index/index.tpl"}

{block name="frontend_index_header"}
    {$toAccount = ($sTarget == "account" || $sTarget == "address")}
    {$smarty.block.parent}
{/block}

{* Title *}
{block name='frontend_index_header_title'}
    {s name="RegisterTitle"}{/s} | {{config name="shopName"}|escapeHtml}
{/block}

{* Back to the shop button *}
{block name='frontend_index_logo_trusted_shops'}
    {$smarty.block.parent}
{/block}

{* Hide breadcrumb *}
{block name='frontend_index_breadcrumb'}
    {if $toAccount}
        {$smarty.block.parent}
    {/if}
{/block}

{* Hide shop navigation *}
{block name='frontend_index_shop_navigation'}
    {if !$theme.checkoutHeader || $toAccount}
        {$smarty.block.parent}
    {/if}
{/block}

{* Step box *}
{block name='frontend_index_navigation_categories_top'}
{/block}

{* Hide top bar *}
{block name='frontend_index_top_bar_container'}
    {if !$theme.checkoutHeader || $toAccount}
        {$smarty.block.parent}
    {/if}
{/block}

{block name="frontend_index_logo_supportinfo"}
{/block}

{* Sidebar left *}
{block name='frontend_index_content_left'}
{/block}

{* Footer *}
{block name="frontend_index_footer"}
{/block}

{* Register content *}
{block name='frontend_index_content'}
    {block name='frontend_register_index_registration_optin_message'}
    {/block}

    {block name='frontend_register_index_registration'}
    {/block}

    {* Register Login *}
    {block name='frontend_register_index_login'}

{namespace name="frontend/account/login"}
<div class="register--login content block" style="width:100%;float:none">

    {* Error messages *}
    {block name='frontend_register_login_error_messages'}
        {if $sErrorMessages}
            <div class="account--error">
                {include file="frontend/register/error_message.tpl" error_messages=$sErrorMessages}
            </div>
        {/if}
    {/block}

    {* New customer *}
    {block name='frontend_register_login_newcustomer'}
    {/block}

    {* Existing customer *}
    {block name='frontend_register_login_customer'}
        <div class="register--existing-customer panel has--border is--rounded">

            {block name='frontend_register_login_customer_title'}
                <h2 class="panel--title is--underline">Login</h2>
            {/block}

            <div class="panel--body is--wide">
                {block name='frontend_register_login_form'}
                    <form name="sLogin" method="post" action="{$formActionUrl}" id="login--form">
                        {block name='frontend_register_login_input_email'}
                            <div class="register--login-email">
                                <input name="email" placeholder="{s name="LoginPlaceholderMail"}{/s}" type="email" autocomplete="email" tabindex="1" value="{$sFormData.email|escape}" id="email" class="register--login-field{if $sErrorFlag.email} has--error{/if}" />
                            </div>
                        {/block}

                        {block name='frontend_register_login_input_form_submit'}
                            <div class="register--login-action">
                                <button type="submit" class="register--login-btn btn is--primary is--large is--icon-right" name="Submit">{s name="LoginLinkLogon"}{/s} <i class="icon--arrow-right"></i></button>
                            </div>
                        {/block}
                    </form>
                {/block}
            </div>

        </div>
    {/block}
</div>

    {/block}

    {* Register advantages *}
    {block name='frontend_register_index_advantages'}
    {/block}

{/block}
