{extends file="frontend/index/index.tpl"}

{block name="frontend_index_header"}
    {$toAccount = ($sTarget == "account" || $sTarget == "address")}
    {$smarty.block.parent}
{/block}

{* Back to the shop button *}
{block name='frontend_index_logo_trusted_shops'}
    {$smarty.block.parent}
{/block}

{* Hide breadcrumb *}
{block name='frontend_index_breadcrumb'}
{/block}

{* Hide shop navigation *}
{block name='frontend_index_shop_navigation'}
{/block}

{* Step box *}
{block name='frontend_index_navigation_categories_top'}
{/block}

{* Hide top bar *}
{block name='frontend_index_top_bar_container'}
{/block}

{block name="frontend_index_logo_supportinfo"}
{/block}

{* Footer *}
{block name="frontend_index_footer"}
{/block}

{block name='frontend_index_sidebar'}
{/block}

                    {* E-Mail *}
                    {block name='frontend_register_personal_fieldset_input_mail'}
                        <div class="register--email">
                            <input autocomplete="section-personal email"
                                   name="register[personal][email]"
                                   type="hidden"
                                   required="required"
                                   aria-required="true"
                                   placeholder="{s name='RegisterPlaceholderMail'}{/s}{s name="RequiredField" namespace="frontend/register/index"}{/s}"
                                   id="register_personal_email"
                                   value="{$email|escape}"
                                   class="register--field email is--required{if isset($error_flags.email)} has--error{/if}" />
                        </div>

                        {if {config name="doubleEmailValidation"}}
                            <div class="register--emailconfirm">
                                <input autocomplete="section-personal email"
                                       name="register[personal][emailConfirmation]"
                                       type="hidden"
                                       required="required"
                                       aria-required="true"
                                       placeholder="{s name='RegisterPlaceholderMailConfirmation'}{/s}{s name="RequiredField" namespace="frontend/register/index"}{/s}"
                                       id="register_personal_emailConfirmation"
                                       value="{$email|escape}"
                                       class="register--field emailConfirmation is--required{if isset($error_flags.emailConfirmation)} has--error{/if}" />
                            </div>
                        {/if}
                    {/block}

{* Register content *}
{block name='frontend_index_content'}
    {block name='frontend_register_index_registration_optin_message'}
        <div class="register--message content block" style="width:100%;float:none">
            <div class="register--headline">
                {block name='frontend_register_index_form_optin_success'}
                    {if $smarty.get.optinsuccess && ({config name="optinregister"} || {config name="optinaccountless"})}
                        {if $isAccountless}
                            {s name="RegisterInfoSuccessOptinAccountless" assign="snippetRegisterInfoSuccessOptinAccountless"}{/s}
                            {include file="frontend/_includes/messages.tpl" type="success" content=$snippetRegisterInfoSuccessOptinAccountless}
                        {else}
                            {s name="RegisterInfoSuccessOptin" assign="snippetRegisterInfoSuccessOptin"}{/s}
                            {include file="frontend/_includes/messages.tpl" type="success" content=$snippetRegisterInfoSuccessOptin}
                        {/if}
                    {/if}
                {/block}
            </div>
        </div>
    {/block}
    {block name='frontend_register_index_registration'}
        <div class="register--content panel content block has--border is--rounded{if $errors.occurred} is--collapsed{/if}" id="registration" data-register="true" style="width:100%;float:none;margin-top:inherit;display:block;">

            {block name='frontend_register_index_dealer_register'}
                {* Included for compatibility reasons *}
            {/block}

            {block name='frontend_register_index_cgroup_header'}
                {if $register.personal.sValidation}
                    {* Include information related to registration for other customergroups then guest, this block get overridden by b2b essentials plugin *}
                    <div class="panel register--supplier">
                        {block name='frontend_register_index_cgroup_header_title'}
                            <h2 class="panel--title is--underline">{$sShopname|escapeHtml} {s name='RegisterHeadlineSupplier' namespace='frontend/register/index'}{/s}</h2>
                        {/block}

                        {block name='frontend_register_index_cgroup_header_body'}
                            <div class="panel--body is--wide">
                                <p class="is--bold">{s name='RegisterInfoSupplier3' namespace='frontend/register/index'}{/s}</p>

                                <h3 class="is--bold">{s name='RegisterInfoSupplier4' namespace='frontend/register/index'}{/s}</h3>
                                <p>{s name='RegisterInfoSupplier5' namespace='frontend/register/index'}{/s}</p>

                                <h3 class="is--bold">{s name='RegisterInfoSupplier6' namespace='frontend/register/index'}{/s}</h3>
                                <p>{s name='RegisterInfoSupplier7' namespace='frontend/register/index'}{/s}</p>
                            </div>
                        {/block}
                    </div>
                {/if}
            {/block}

            {block name='frontend_register_index_form'}
                <form method="post" action="{$formActionUrl}" class="panel register--form" id="register--form">

                    {* Invalid hash while option verification process *}
                    {block name='frontend_register_index_form_optin_invalid_hash'}
                        {if $smarty.get.optinhashinvalid && ({config name="optinregister"} || {config name="optinaccountless"})}
                            {s name="RegisterInfoInvalidHash" assign="snippetRegisterInfoInvalidHash"}{/s}
                            {include file="frontend/_includes/messages.tpl" type="error" content=$snippetRegisterInfoInvalidHash}
                        {/if}
                    {/block}

                    {block name='frontend_register_index_form_captcha_fieldset'}
                        {include file="frontend/register/error_message.tpl" error_messages=$errors.captcha}
                    {/block}

                    {block name='frontend_register_index_form_personal_fieldset'}
                        {include file="frontend/register/error_message.tpl" error_messages=$errors.personal}
                        {include file="frontend/register/personal_fieldset.tpl" form_data=$register.personal error_flags=$errors.personal fieldset_title='Register'}
                    {/block}

                    {block name='frontend_register_index_form_billing_fieldset'}
                        {include file="frontend/register/error_message.tpl" error_messages=$errors.billing}
                        {include file="frontend/register/billing_fieldset.tpl" form_data=$register.billing error_flags=$errors.billing country_list=$countryList update='1'}
                    {/block}

                {* Alternative *}
                {block name='frontend_register_billing_fieldset_different_shipping'}

                {/block}

                    {block name='frontend_register_index_form_shipping_fieldset'}
                    {/block}

                    {block name='frontend_register_index_form_required'}
                        {* Required fields hint *}
                        <div class="register--required-info required_fields">
                            {s name='RegisterPersonalRequiredText' namespace='frontend/register/personal_fieldset'}{/s}
                        </div>
                    {/block}

                    {* Captcha *}
                    {block name='frontend_register_index_form_captcha'}
                        {$captchaName = {config name="registerCaptcha"}}
                        {$captchaHasError = $errors.captcha}
                        {include file="widgets/captcha/custom_captcha.tpl" captchaName=$captchaName captchaHasError=$captchaHasError}
                    {/block}

                    {block name='frontend_register_index_form_submit'}
                        {* Submit button *}
                        <div class="register--action">
                            <button type="submit" class="register--submit btn is--primary is--large is--icon-right" name="Submit" data-preloader-button="true">{s name="RegisterIndexNewActionSubmit" namespace="frontend/register/index"}{/s} <i class="icon--arrow-right"></i></button>
                        </div>
                    {/block}
                </form>
            {/block}
        </div>
    {/block}

{/block}