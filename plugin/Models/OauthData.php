<?php

namespace HelloAgainConnector\Models;

use Symfony\Component\Validator\Constraints as Assert,
    Doctrine\Common\Collections\ArrayCollection,
    Shopware\Components\Model\ModelEntity,
    Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="OauthDataRepository")
 * @ORM\Table(name="hla_oauth_data")
 */
class OauthData extends ModelEntity
{
    /**
     *
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     *
     * @ORM\Column(name="customer_id", type="integer", nullable=true)
     */
    protected $customerId;

    /**
     *
     * @ORM\Column(name="scope", type="string", nullable=true)
     */
    protected $scope;

    /**
     *
     * @ORM\Column(name="code", type="string", nullable=true)
     */
    protected $code;

    /**
     *
     * @ORM\Column(name="code_created_at", type="datetime", nullable=true)
     */
    protected $codeCreatedAt;

    /**
     *
     * @ORM\Column(name="access_token", type="text", nullable=true)
     */
    protected $accessToken;

    /**
     *
     * @ORM\Column(name="access_token_created_at", type="datetime", nullable=true)
     */
    protected $accessTokenCreatedAt;

    public function getId()
    {
        return $this->id;
    }

    public function getCustomerId()
    {
        return $this->customerId;
    }

    public function getScope()
    {
        return $this->scope;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getCodeCreatedAt()
    {
        return $this->codeCreatedAt;
    }

    public function getAccessToken()
    {
        return $this->accessToken;
    }

    public function getAccessTokenCreatedAt()
    {
        return $this->accessTokenCreatedAt;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
        return $this;
    }

    public function setScope($scope)
    {
        $this->scope = $scope;
        return $this;
    }

    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    public function setCodeCreatedAt($codeCreatedAt)
    {
        $this->codeCreatedAt = $codeCreatedAt;
        return $this;
    }

    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
        return $this;
    }

    public function setAccessTokenCreatedAt($accessTokenCreatedAt)
    {
        $this->accessTokenCreatedAt = $accessTokenCreatedAt;
        return $this;
    }
}
