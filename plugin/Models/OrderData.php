<?php

namespace HelloAgainConnector\Models;

use Symfony\Component\Validator\Constraints as Assert,
    Doctrine\Common\Collections\ArrayCollection,
    Shopware\Components\Model\ModelEntity,
    Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="OrderDataRepository")
 * @ORM\Table(name="hla_order_data")
 */
class OrderData extends ModelEntity
{
    /**
     *
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     *
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     */
    protected $orderId;

    /**
     *
     * @ORM\Column(name="helloagain_id", type="string", nullable=true)
     */
    protected $helloagainId;

    /**
     *
     * @ORM\Column(name="synced_at", type="datetime", nullable=true)
     */
    protected $syncedAt;

    /**
     *
     * @ORM\Column(name="sync_count", type="integer", nullable=true)
     */
    protected $syncCount;


    public function getId()
    {
        return $this->id;
    }

    public function getOrderId()
    {
        return $this->orderId;
    }

    public function getHelloagainId()
    {
        return $this->helloagainId;
    }

    public function getSyncedAt()
    {
        return $this->syncedAt;
    }

    public function getSyncCount()
    {
        return $this->syncCount;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
        return $this;
    }

    public function setHelloagainId($helloagainId)
    {
        $this->helloagainId = $helloagainId;
        return $this;
    }

    public function setSyncedAt($syncedAt)
    {
        $this->syncedAt = $syncedAt;
        return $this;
    }

    public function setSyncCount($syncCount)
    {
        $this->syncCount = $syncCount;
        return $this;
    }
}
