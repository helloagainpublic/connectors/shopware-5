<?php

namespace HelloAgainConnector\Models;

use Shopware\Components\Model\ModelRepository;
use HelloAgainConnector\Models\OrderData;

/**
 * Class HelloAgainConnectorModelRepository
 * @package HelloAgainConnector\Models
 */
class OrderDataRepository extends ModelRepository
{
    /**
     * @param string $orderId
     * @param string $helloagainId
     * @return object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function create($orderId, $helloagainId)
    {
        $orderData = new OrderData;
        $orderData
            ->setOrderId($orderId)
            ->setHelloagainId($helloagainId);

        $this->save($orderData);

        return $orderData;
    }

    /**
     * @param OrderData $orderData
     * @param string $accessToken
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveSyncInfo($orderData, $succeeded = true)
    {
        if ($succeeded) {
            $orderData->setSyncedAt(new \DateTime());
        } else {
            $orderData->setSyncCount($orderData->getSyncCount() + 1);
        }

        $this->save($orderData);
    }

    /**
     * @param OrderData $orderData
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save($orderData)
    {
        $this->getEntityManager()->persist($orderData);
        $this->getEntityManager()->flush();
    }
}
