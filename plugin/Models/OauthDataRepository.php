<?php

namespace HelloAgainConnector\Models;

use Shopware\Components\Model\ModelRepository;
use HelloAgainConnector\Models\OauthData;

/**
 * Class HelloAgainConnectorModelRepository
 * @package HelloAgainConnector\Models
 */
class OauthDataRepository extends ModelRepository
{
    /**
     * @param OauthData $oauthData
     * @param string $code
     * @return object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function create($customerId, $scope, $code)
    {
        $oauthData = new OauthData;
        $oauthData
            ->setCustomerId($customerId)
            ->setScope($scope)
            ->setCode($code)
            ->setCodeCreatedAt(new \DateTime());

        $this->save($oauthData);

        return $oauthData;
    }

    /**
     * @param string $code
     * @return object|null
     */
    public function loadByCode($code)
    {
        return $this->findOneBy(['code' => $code]);
    }

    /**
     * @param OauthData $oauthData
     * @param string $accessToken
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveAccessToken($oauthData, $accessToken)
    {
        $oauthData->setAccessToken($accessToken);
        $oauthData->setAccessTokenCreatedAt(new \DateTime());

        $this->save($oauthData);
    }

    /**
     * @param OauthData $oauthData
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save($oauthData)
    {
        $this->getEntityManager()->persist($oauthData);
        $this->getEntityManager()->flush();
    }
}
