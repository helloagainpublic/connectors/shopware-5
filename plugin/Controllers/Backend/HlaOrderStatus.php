<?php
use Shopware\Models\Order\Order;

class Shopware_Controllers_Backend_HlaOrderStatus extends Shopware_Controllers_Backend_ExtJs 
{
    public static $manager;
    protected $orderRepository;

    public function initstatusAction()
    {
        $orderStatuses = $this->getOrderRepository()->getOrderStatusQuery()->getArrayResult();

        $orderStatusList = [];
        foreach ($orderStatuses as $orderStatus) {
            $orderStatusName = ucfirst(implode(" ",explode("_", $orderStatus['name'])));
            $orderStatusList[] = [
                'id' => $orderStatus['id'],
                'name' => $orderStatusName
            ];
        }
        $this->View()->assign([
            'data' => $orderStatusList,
            'total' => count($orderStatusList),
        ]);
    }

    private function getOrderRepository()
    {
        if ($this->orderRepository === null) {
            $this->orderRepository = $this->getManager()->getRepository(Order::class);
        }

        return $this->orderRepository;
    }

    protected function getManager()
    {
        if (self::$manager === null) {
            self::$manager = $this->get('models');
        }

        return self::$manager;
    }
}
