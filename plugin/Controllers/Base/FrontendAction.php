<?php

namespace HelloAgainConnector\Controllers\Base;

use Shopware\Components\Plugin;

class FrontendAction extends \Enlight_Controller_Action
{
    /**
     * @var Plugin $plugin
     */
    protected $plugin;

    protected $helper;

    protected $customerHelper;

    public function preDispatch()
    {
        $this->helper = $this->container->get('hla.datahelper');
        $this->customerHelper = $this->container->get('hla.customerhelper');

        $this->plugin = $this->get('kernel')->getPlugins()['HelloAgainConnector'];
        $this->get('template')->addTemplateDir($this->plugin->getPath() . '/Resources/views/');
    }

    protected function setJsonResponse($code, $data = null)
    {
        $this->Front()->Plugins()->ViewRenderer()->setNoRender();
        $this->Response()->setHeader('content-type', 'application/json', true);
        $this->Response()->setHttpResponseCode($code);

        if ($data) {
            $this->Response()->setBody(json_encode($data));
        }
    }

    protected function ensureOauth2Session()
    {
        if (Shopware()->Session()->get('hlaOauth2RedirectUri') == null) {
            $this->setJsonResponse(401);
        }
    }

    public function redirectExternal($url, array $options = [])
    {
        $this->Response()->setRedirect($url, empty($options['code']) ? 302 : (int) $options['code']);
    }
}
