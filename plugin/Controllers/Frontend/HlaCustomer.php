<?php

use HelloAgainConnector\Controllers\Base\FrontendAction;
use Shopware\Models\Customer\Customer;
use Shopware\Bundle\AccountBundle\Form\Account\PersonalFormType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Shopware\Models\Customer\Address;
use Shopware\Bundle\AccountBundle\Form\Account\AddressFormType;
use Shopware\Bundle\StoreFrontBundle\Gateway\CountryGatewayInterface;


class Shopware_Controllers_Frontend_HlaCustomer extends FrontendAction implements \Shopware\Components\CSRFWhitelistAware
{
    protected $helper;

    protected $customerHelper;

    /**
     * Whitelist actions
     *
     * @return string[]
     */
    public function getWhitelistedCSRFActions()
    {
        return [
            'indexpost',
            'loginpost',
            'registerpost'
        ];
    }

    public function indexAction()
    {
        $this->ensureOauth2Session();

        $url = $this->Front()->Router()->assemble([
            'module' => 'frontend',
            'controller' => 'hlacustomer',
            'action' => 'indexpost',
        ]);

        $this->View()->assign('formActionUrl', $url);
    }

    public function indexPostAction()
    {
        $this->ensureOauth2Session();

        $email = $this->Request()->getParam('email');

        if ($email) {
            if ($this->customerHelper->loadCustomerByEmail($email)) {
                return $this->redirect([
                    'module' => 'frontend',
                    'controller' => 'hlacustomer',
                    'action' => 'login',
                    'email' => $email,
                ]);
            }
        }

        return $this->redirect([
            'module' => 'frontend',
            'controller' => 'hlacustomer',
            'action' => 'register',
            'email' => $email,
        ]);
    }

    public function loginAction()
    {
        $this->ensureOauth2Session();

        $url = $this->Front()->Router()->assemble([
            'module' => 'frontend',
            'controller' => 'hlacustomer',
            'action' => 'loginpost',
        ]);

        $this->View()->assign('email', $this->Request()->getParam('email'));
        $this->View()->assign('formActionUrl', $url);
    }

    public function loginPostAction()
    {
        $this->ensureOauth2Session();

        if (!$this->request->isPost()) {
            $this->forward('login');
            return;
        }

        $email = $this->Request()->getParam('email');
        $password = $this->Request()->getParam('password');

        if ($email && $password) {
            $customer = $this->customerHelper->authenticate($email, $password);
            if ($customer) {
                $code = $this->helper->generateOauth2Code($customer->getId());

                return $this->redirect([
                    'module' => 'frontend',
                    'controller' => 'hlaoauth2',
                    'action' => 'redirect',
                    'code' => $code,
                ]);
            }
        }

        return $this->redirect([
            'module' => 'frontend',
            'controller' => 'hlacustomer',
            'action' => 'login',
            'email' => $email,
        ]);
    }

    public function registerAction()
    {
        $this->ensureOauth2Session();

        $url = $this->Front()->Router()->assemble([
            'module' => 'frontend',
            'controller' => 'hlacustomer',
            'action' => 'registerpost',
        ]);

        $this->View()->assign('email', $this->Request()->getParam('email'));
        $this->View()->assign('formActionUrl', $url);
        $this->View()->assign('countryList', $this->get('modules')->Admin()->sGetCountryList());
    }

    public function registerPostAction()
    {
        $this->ensureOauth2Session();

        if (!$this->request->isPost()) {
            $this->forward('register');
            return;
        }

        //print_r($this->Request()->getParams());exit;

        /** @var ShopContextInterface $context */
        $context = $this->get(\Shopware\Bundle\StoreFrontBundle\Service\ContextServiceInterface::class)->getShopContext();

        /** @var Enlight_Components_Session_Namespace $session */
        $session = $this->get('session');

        /** @var RegisterServiceInterface $registerService */
        $registerService = $this->get(\Shopware\Bundle\AccountBundle\Service\RegisterServiceInterface::class);

        $data = $this->getPostData();

        $customerForm = $this->createCustomerForm($data['register']['personal']);
        $billingForm = $this->createBillingForm($data['register']['billing']);

        $errors = [
            'personal' => $this->getFormErrors($customerForm),
            'billing' => $this->getFormErrors($billingForm),
            'shipping' => [],
            'captcha' => [],
        ];

        $shipping = null;
        if ($this->isShippingProvided($data)) {
            $shippingForm = $this->createShippingForm($data['register']['shipping']);
            $shipping = $shippingForm->getData();
            $errors['shipping'] = $this->getFormErrors($shippingForm);
        } else {
            /** @var Address $billing */
            $billing = $billingForm->getData();

            $billingCountry = $billing->getCountry();

            if ($billingCountry === null) {
                throw new RuntimeException('Billing address needs a country');
            }

            $country = $this->get(CountryGatewayInterface::class)->getCountry($billingCountry->getId(), $context);

            if (!$country->allowShipping()) {
                $errors['billing']['country'] = $this->get('snippets')->getNamespace('frontend/register/index')->get('CountryNotAvailableForShipping');
            }
        }

        $errors['occurred'] = !empty($errors['personal'])
            || !empty($errors['shipping'])
            || !empty($errors['billing'])
            || !empty($errors['captcha']);

        if ($errors['occurred']) {
            $this->handleRegisterError($data, $errors);
            return;
        }

        /** @var Customer $customer */
        $customer = $customerForm->getData();

        /** @var Address $billing */
        $billing = $billingForm->getData();

        $config = $this->container->get(\Shopware_Components_Config::class);

        $accountMode = (int) $customer->getAccountMode();
        $doubleOptinWithAccount = ($accountMode === 0) && $config->get('optinregister');
        $doubleOptInAccountless = ($accountMode === 1) && $config->get('optinaccountless');

        $doubleOptinRegister = $doubleOptinWithAccount || $doubleOptInAccountless;
        $shop = $context->getShop();
        /*$shop->addAttribute('sendOptinMail', new Attribute([
            'sendOptinMail' => $doubleOptinRegister,
        ]));*/

        $customer->setReferer((string) $session->offsetGet('sReferer'));
        $customer->setValidation((string) $data['register']['personal']['sValidation']);
        $customer->setAffiliate((int) $session->offsetGet('sPartner'));
        $customer->setPaymentId((int) $session->offsetGet('sPaymentID'));
        $customer->setDoubleOptinRegister($doubleOptinRegister);
        $customer->setDoubleOptinConfirmDate(null);

        /** @var Enlight_Event_EventManager $eventManager */
        $eventManager = $this->get('events');

        $errors = ['occurred' => false];
        $errors = $eventManager->filter(
            'Shopware_Modules_Admin_SaveRegister_BeforeRegister',
            $errors,
            [
                'customer' => $customer,
                'billing' => $billing,
                'shipping' => $shipping,
            ]
        );

        if ($errors['occurred']) {
            $this->handleRegisterError($data, $errors);

            return;
        }

        $registerService->register(
            $shop,
            $customer,
            $billing,
            $shipping
        );

        /*
            * Remove sensitive data before writing to the session
            */
        /*unset(
            $data['register']['personal']['password'],
            $data['register']['personal']['passwordConfirmation'],
            $data['register']['billing']['password'],
            $data['register']['billing']['passwordConfirmation']
        );

        if ($doubleOptinRegister) {
            $eventManager->notify(
                'Shopware_Modules_Admin_SaveRegister_DoubleOptIn_Waiting',
                [
                    'id' => $customer->getId(),
                    'billingID' => $customer->getDefaultBillingAddress()->getId(),
                    'shippingID' => $customer->getDefaultShippingAddress()->getId(),
                ]
            );

            $session->offsetSet('isAccountless', $accountMode === Customer::ACCOUNT_MODE_FAST_LOGIN);

            $this->redirectCustomer([
                'location' => 'register',
                'optinsuccess' => true,
            ]);

            return;
        }*/

        $this->saveRegisterSuccess($data, $customer);

        $code = $this->helper->generateOauth2Code($customer->getId());

        return $this->redirect([
            'module' => 'frontend',
            'controller' => 'hlaoauth2',
            'action' => 'redirect',
            'code' => $code,
        ]);
    }

    public function sessionInitAction()
    {
        $accessToken = $this->Request()->getParam('helloagain_token');
        $redirectUri = $this->Request()->getParam('redirect_uri');

        $userData = $this->helper->getUserData($accessToken);

        if (isset($userData['id'])) {
            $authenticateUser = $this->helper->getUserDataByHlaId($userData['id']);
            if (isset($authenticateUser['identifier'])) {
                $customerId = $authenticateUser['identifier'];
                $customer = $this->customerHelper->loadCustomerById($customerId);
                if ($customer) {
                    $this->customerHelper->saveHelloagainId($customer, $userData['id']);
                    $this->customerHelper->setCustomerDataAsLoggedIn($customer);
                } else {
                    $this->get('corelogger')->error("HelloAgain: Customer with Id [$customerId] does not exist.");
                }
            }
        } else {
            $this->get('corelogger')->error("HelloAgain: Unnexpected API response. Missing 'id' parameter");
        }

        return !empty($redirectUri) ?
            $this->redirectExternal($redirectUri) :
            $this->redirect([
               'module' => 'frontend',
               'controller' => 'index',
            ]);
    }

    private function getPostData()
    {
        $data = $this->request->getPost();

        $countryStateName = 'country_state_' . $data['register']['billing']['country'];
        $data['register']['billing']['state'] = $data['register']['billing'][$countryStateName];

        $countryStateName = 'country_shipping_state_' . $data['register']['shipping']['country'];
        $data['register']['shipping']['state'] = $data['register']['shipping'][$countryStateName];
        $data['register']['billing'] += $data['register']['personal'];
        $data['register']['shipping']['phone'] = $data['register']['personal']['phone'];

        if (!$data['register']['personal']['accountmode']) {
            $data['register']['personal']['accountmode'] = Customer::ACCOUNT_MODE_CUSTOMER;
        }

        $data['register']['billing']['additional']['customer_type'] = $data['register']['personal']['customer_type'];

        return $data;
    }

    private function createCustomerForm(array $data)
    {
        $customer = new Customer();
        $form = $this->createForm(PersonalFormType::class, $customer);
        $form->submit($data);

        return $form;
    }

    private function createBillingForm(array $data)
    {
        $address = new Address();
        $form = $this->createForm(AddressFormType::class, $address);
        $form->submit($data);

        return $form;
    }

    private function createShippingForm(array $data)
    {
        $address = new Address();
        $form = $this->createForm(AddressFormType::class, $address);
        $form->submit($data);

        return $form;
    }

    /**
     * @return array
     */
    private function getCountries()
    {
        $context = $this->get(\Shopware\Bundle\StoreFrontBundle\Service\ContextServiceInterface::class)->getShopContext();
        $service = $this->get(\Shopware\Bundle\StoreFrontBundle\Service\LocationServiceInterface::class);
        $countries = $service->getCountries($context);

        return $this->get(\Shopware\Components\Compatibility\LegacyStructConverter::class)->convertCountryStructList($countries);
    }

    private function sendRegistrationMail(Customer $customer)
    {
        try {
            Shopware()->Modules()->Admin()->sSaveRegisterSendConfirmation($customer->getEmail());
        } catch (\Exception $e) {
            $message = sprintf('Could not send user registration email to address %s', $customer->getEmail());
            $this->get('corelogger')->error('HelloAgain: ' . $message, ['exception' => $e->getMessage()]);
        }
    }

    private function handleRegisterError(array $data, array $errors)
    {
        unset(
            $data['register']['personal']['password'],
            $data['register']['personal']['passwordConfirmation'],
            $data['register']['personal']['emailConfirmation']
        );

        $this->View()->assign('errors', $errors);
        $this->View()->assign($data);
        $this->forward('index');
    }

    private function getFormErrors(FormInterface $form)
    {
        if ($form->isSubmitted() && $form->isValid()) {
            return [];
        }
        $errors = [
            '' => $this->get('snippets')
                ->getNamespace('frontend/account/internalMessages')
                ->get('ErrorFillIn', 'Please fill in all red fields'),
        ];

        foreach ($form->getErrors(true) as $error) {
            $errors[$error->getOrigin()->getName()] = $this->View()->fetch('string:' . $error->getMessage());
        }

        return $errors;
    }

    private function isShippingProvided(array $data)
    {
        return \array_key_exists('shippingAddress', $data['register']['billing']);
    }

    private function saveRegisterSuccess(array $data, Customer $customer)
    {
        /** @var Enlight_Event_EventManager $eventManager */
        $eventManager = $this->get('events');

        //$this->writeSession($data, $customer);
        //$this->loginCustomer($customer);

        if ($customer->getAccountMode() == Customer::ACCOUNT_MODE_CUSTOMER) {
            $this->sendRegistrationMail($customer);
        }

        $eventManager->notify(
            'Shopware_Modules_Admin_SaveRegister_Successful',
            [
                'id' => $customer->getId(),
                'billingID' => $customer->getDefaultBillingAddress()->getId(),
                'shippingID' => $customer->getDefaultShippingAddress()->getId(),
            ]
        );
    }
}
