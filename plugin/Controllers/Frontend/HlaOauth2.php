<?php

use HelloAgainConnector\Controllers\Base\FrontendAction;
use Shopware\Models\Customer\Customer;
use HelloAgainConnector\Models\OauthData;
use HelloAgainConnector\Service\DataHelper;

class Shopware_Controllers_Frontend_HlaOauth2 extends FrontendAction implements \Shopware\Components\CSRFWhitelistAware
{
    /**
     * Whitelist actions
     *
     * @return string[]
     *
     * @psalm-return array{0: 'return'}
     */
    public function getWhitelistedCSRFActions()
    {
        return [
            'token'
        ];
    }

    public function authorizeAction()
    {
        $responseType = $this->Request()->getParam('response_type');
        $clientId = $this->Request()->getParam('client_id');
        $redirectUri = $this->Request()->getParam('redirect_uri');
        $scope = $this->Request()->getParam('scope');
        $state = $this->Request()->getParam('state');

        if ($responseType != 'code' || !isset($scope)  || $clientId != $this->helper->getClientId()) {
            return $this->setJsonResponse(401);
        }

        Shopware()->Session()->offsetSet('hlaOauth2RedirectUri', $redirectUri);
        Shopware()->Session()->offsetSet('hlaOauth2Scope', $scope);
        Shopware()->Session()->offsetSet('hlaOauth2State', $state);

        if ($this->helper->getLoginPage() == DataHelper::DEFAULT_LOGIN) {
            $userId = Shopware()->Session()->offsetGet('sUserId');
            if ($userId) {
                $code = $this->helper->generateOauth2Code($userId);
                return $this->redirect([
                    'module' => 'frontend',
                    'controller' => 'hlaoauth2',
                    'action' => 'redirect',
                    'code' => $code,
                ]);
            }
            return $this->redirect([
                'module' => 'frontend',
                'controller' => 'account',
                'action' => 'index'
            ]);
            exit;
        }

        return $this->redirect([
            'module' => 'frontend',
            'controller' => 'hlacustomer',
            'action' => 'index',
        ]);
    }

    public function redirectAction()
    {
        $redirectUri = Shopware()->Session()->get('hlaOauth2RedirectUri');
        $state = Shopware()->Session()->get('hlaOauth2State');
        $code = $this->Request()->getParam('code');

        return $this->redirectExternal(implode('?', [$redirectUri, http_build_query([
            'state' => $state,
            'code' => $code,
        ])]));
    }

    public function tokenAction()
    {
        $code = $this->Request()->getParam('code');
        $clientId = $this->Request()->getParam('client_id');
        $clientSecret = $this->Request()->getParam('client_secret');
        $grantType = $this->Request()->getParam('grant_type');

        if ($clientId != $this->helper->getClientId() || $clientSecret != $this->helper->getClientSecret() || $grantType != 'authorization_code') {
            $this->get('corelogger')->error("HelloAgain: Invalid Oauth2 token request for code: $code");
            return $this->setJsonResponse(401);
        }

        $oauthDataRepository = $this->container->get('models')->getRepository(OauthData::class);
        $oauthData = $oauthDataRepository->loadByCode($code);

        if (!$oauthData) {
            $this->get('corelogger')->error("HelloAgain: Missing Oauth2 data for code: $code");
            return $this->setJsonResponse(401);
        }

        $customer = $this->customerHelper->loadCustomerById($oauthData->getCustomerId());

        try {
            $this->customerHelper->initHelloagainId($customer);
        } catch (\Exception $e) {
            $this->get('corelogger')->error("HelloAgain: Oauth2 token customer init - exception: " . $e->getMessage());
            return $this->setJsonResponse(400);
        };

        $accessToken = $this->helper->generateOauth2Token($customer);

        $oauthDataRepository->saveAccessToken($oauthData, $accessToken);

        $jsonData = [
            'access_token' => $accessToken,
            'token_type' => 'bearer',
            'expires_in' => 3600 * 24 * 365,
            'scope' => $oauthData->getScope()
        ];

        return $this->setJsonResponse(200, $jsonData);
    }
}
