# Shopware 5 Connector

Shopware 5 connector for hello again

## Release

```
./package.sh
```

## Installation

#### Cron Jobs
Cron Jobs must be enabled on the Shop, in order for the plugin to work: https://docs.shopware.com/en/shopware-5-en/settings/system-cronjobs
In case Cron Jobs are not configured, they can be set up like:

```
cd /var/www/<SHOP>/web
php bin/console sw:cron:list

1. Execute all pending cronjobs
php bin/console sw:cron:run

2. Execute only helloagain cron:
php bin/console sw:cron:run Shopware_CronJob_HelloAgainDataExport

3. Trigger cronjobs via web url
*/15 * * * * wget -q http://www.myshop.com/backend/cron
```

## Configuration

```
Oauth2 Authorize URI: BASEURL/hlaoauth2/authorize
Oauth2 Exchange Token URI: BASEURL/hlaoauth2/token
Webview Init URI: BASEURL/hlacustomer/sessioninit?helloagain_token={{externalToken}}
Webview with Redirect: BASEURL/hlacustomer/sessioninit?helloagain_token={{externalToken}}&redirect_uri={{redirectUrl}}
```

## Getting started

* Download the latest released module as a ZIP file from [here](https://gitlab.com/helloagainpublic/connectors/shopware-5/uploads/f8f35b65468e676a3b87722862beac85/helloagain-shopware5-1.0.0.zip)
* Extract the ZIP file to `custom/plugins` 
* Activate the plugin via `Backend Configuration > Plugin Manager`
* Adjust the configuration via `Backend Configuration > Basic Settings > Additional Settings > hello again`
